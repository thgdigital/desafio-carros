//
//  Helper.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit

struct App {
   static let baseURL = "http://desafiobrq.herokuapp.com/v1/"
}


// MARK: - UIColor
extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}

extension NumberFormatter {
    convenience init(style: Style) {
        self.init()
        numberStyle = style
    }
}


extension Formatter {
    static let currency = NumberFormatter(style: .currency)
    static let currencyUS: NumberFormatter = {
        let formatter = NumberFormatter(style: .currency)
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()
    static let currencyBR: NumberFormatter = {
        let formatter = NumberFormatter(style: .currency)
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter
    }()
}
extension FloatingPoint {
    var currency: String {
        return Formatter.currency.string(for: self) ?? ""
    }
    var currencyUS: String {
        return Formatter.currencyUS.string(for: self) ?? ""
    }
    var currencyBR: String {
        return Formatter.currencyBR.string(for: self) ?? ""
    }
    
}
extension Int {
    var CGFloatValue: CGFloat {
        get {
            return CGFloat(self)
        }
    }
    var floatvalue: Float{
        get {
            return Float(self)
        }
        
    }
}
extension UIViewController {
    
    func addLeftBarButtonItem(_ image: UIImage?, target: Any, action: Selector) {
        
        let barButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: target, action: action)
        navigationItem.setLeftBarButton(barButtonItem, animated: true)
        navigationItem.leftBarButtonItem?.isEnabled = true
    }
    func buttonImageForEmptyState() -> UIImage {
        let capInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
        
        let buttonWidth: CGFloat = Device.isPad() ? 400 : 300
        let padding: CGFloat = -((Device.currentDeviceWidth - buttonWidth)/2)
        let rectInsets = UIEdgeInsets(top: -19, left: padding, bottom: -19, right: padding)
        return UIImage(named: "borderButton")!.resizableImage(withCapInsets: capInsets, resizingMode: UIImageResizingMode.stretch).withAlignmentRectInsets(rectInsets)
    }
    
    func addRightBarButtonItem(_ image: UIImage?, target: Any, action: Selector) {
        
        let barButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: target, action: action)
        navigationItem.setRightBarButton(barButtonItem, animated: true)
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func setupNavigationBarBackButton() {
        navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "arrow_menu_close"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(UIViewController.back(_:)))
        navigationItem.leftBarButtonItem = newBackButton;
    }
    
    @objc func back(_ sender: UIBarButtonItem) {
        
        
        
        if presentingViewController == nil {
            navigationController?.popViewController(animated: true)
            return
        }
        
        dismiss(animated: true, completion: nil)
    }
}

extension UIAlertController {
    
    func show(_ viewController: UIViewController? = nil) {
        
        let topViewController = viewController
        
        topViewController?.present(self, animated: true, completion: nil)
    }
    
    static func showAlert(title: String, message: String, cancelButtonTitle: String? = nil, confirmationButtonTitle: String? = nil, viewController: UIViewController? = nil, dismissBlock: (()-> Void)? = nil , cancelBlock: (()-> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        // Cancel Button
        if cancelButtonTitle != nil {
            alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                cancelBlock?()
            }))
        }
        // Confirmation button
        if confirmationButtonTitle != nil {
            alert.addAction(UIAlertAction(title: confirmationButtonTitle, style: UIAlertActionStyle.default, handler: { (action) -> Void in
                dismissBlock?()
            }))
        }
        
        // Show
        DispatchQueue.main.async {
            alert.show(viewController)
        }
    }
}

