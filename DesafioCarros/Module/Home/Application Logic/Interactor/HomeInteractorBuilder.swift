//
//  HomeInteractorBuilder.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

class HomeInteractorBuilder {
    
    static func create() -> HomeInteractor {
        let manager = HomeManager()
        let managerDB = DBManager.shared
        let interactor = HomeInteractor(manager: manager, managerBD: DBManager.shared)
        manager.output = interactor
        managerDB.output = interactor
        return interactor
    }
}
