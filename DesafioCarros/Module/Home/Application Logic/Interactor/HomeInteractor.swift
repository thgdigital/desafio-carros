//
//  HomeInteractor.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

protocol HomeInteractorInput {
    var carrosEntity: [CarroEntity] { get }
    func fetch()
    func pagination()
    func comprar(carro: CarroEntity)
}
protocol HomeInteractorOutput: class {
    func fetched(carrosItem: [CarroItem])
    func finishPagination()
    func requestError()
    func insertSucess()
    func insertError()
}
class HomeInteractor: HomeInteractorInput {
    
    var manager: HomeManagerInput
    var managerBD: DBManager
    weak var output: HomeInteractorOutput?
    var carrosEntity: [CarroEntity] = [CarroEntity]()
    
    init(manager: HomeManager, managerBD: DBManager) {
        self.managerBD = managerBD
        self.manager = manager
    }
    
    func fetch() {
        self.manager.fecth()
    }
    
    func pagination(){
        
        self.manager.paginate()
    }
    
    func comprar(carro: CarroEntity) {
        
        let  itemIntity = CarroMapper.convertItemWidthCarroEntity(carroEntity: carro)
        self.managerBD.saveObj(itemIntity)
    }
}
extension HomeInteractor: DBManagerOuput{
    
    func didModification(status: Bool) {
        
        if status{
            self.output?.insertSucess()
        }else{
            self.output?.insertError()
        }
    }
}
extension HomeInteractor: HomeManagerOutput {
    
    func fecthed(carrosEntity: [CarroEntity]) {
        self.carrosEntity = carrosEntity
        self.output?.fetched(carrosItem:  self.carrosEntity.map({ CarroMapper.homeListHomeEntity(carroEntity: $0)}))
    }
    
    func requestError() {
        self.output?.requestError()
    }
    
    func fecthedPaginate() {
        self.output?.finishPagination()
    }
    
}
