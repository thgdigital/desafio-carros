//
//  HomeManager.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public enum EndPointer: String {
    case carro = "carro"
    
}
protocol HomeManagerInput {
    func fecth()
    func paginate()
}

protocol HomeManagerOutput: class {
    func fecthed(carrosEntity: [CarroEntity])
    func requestError()
    func fecthedPaginate()
}
class HomeManager: HomeManagerInput {
       
    // MARK: - Controles da Paginação
    fileprivate var nextPageURL: String?
    
    weak var output : HomeManagerOutput?
    
    func fecth() {
        let url = assembleURL(endpoint: .carro)
         _ = Alamofire.request(url, method: .get).validate().responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                guard let jsonObject = response.result.value else {
                        self.output?.requestError()
                    return
                }
                
                let json                = JSON(jsonObject)
                let jsonArray           = json.arrayValue
                self.output?.fecthed(carrosEntity: jsonArray.map({return CarroMapper.homeListHomeEntity(json: $0)}))
                
            case .failure( _):
                self.output?.requestError()
                
            }
        }
        
    }
    
    // MARK: - Assemble URL
    internal func assembleURL(endpoint: EndPointer) -> String {
        let  url = "\(App.baseURL)/\(endpoint.rawValue)"
        return url
    }
    
    
    // TODO: - Criar request de paginação
    func paginate() {
      self.output?.fecthedPaginate()
    }
}
