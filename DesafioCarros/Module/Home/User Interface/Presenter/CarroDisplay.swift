//
//  CarroDisplay.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

struct CarroDisplay {
    var id: Int = 0
    var name: String = ""
    var descricao: String = ""
    var marca: String = ""
    var preco: String = ""
    var image: String = ""
    var qtd: Int = 0
}
