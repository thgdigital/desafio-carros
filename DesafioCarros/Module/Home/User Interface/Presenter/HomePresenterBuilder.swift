//
//  HomePresenterBuilder.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

class HomePresenterBuilder {
    static func create(wireframe: HomeWireframe) -> HomePresenter {
        
        let interactor = HomeInteractorBuilder.create()
        let presenter = HomePresenter(interactor: interactor, wireframe: wireframe)
        interactor.output = presenter
        
        return presenter
        
    }
}
