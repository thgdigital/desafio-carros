//
//  HomePresenter.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

protocol HomePresenterInput {
    func viewDidLoad()
    func comprar(_ index: Int, qtd: Int)
    func didSected(id: Int)
    func paginate()
    func openCarrinho()
}

protocol HomePresenterOutput: class {
    func load()
    func resultCarros(carros: [CarroDisplay])
    func didPagination()
    func hideLoading()
    func error()
    func addCarrinho()
    func insertError()
}

class HomePresenter:  HomePresenterInput {
    
    var interactor: HomeInteractorInput
    var wireframe: HomeWireframe
    weak var output: HomePresenterOutput?
    
    init(interactor: HomeInteractorInput, wireframe: HomeWireframe) {
        
        self.interactor = interactor
        
        self.wireframe = wireframe
    }
    
    func viewDidLoad() {
        
        self.output?.load()
        
        self.interactor.fetch()
    }
    
    func comprar(_ index: Int, qtd: Int) {
        let carroEntity = self.interactor.carrosEntity[index]
        
        carroEntity.qtd = qtd
        
        self.output?.load()
        
        self.interactor.comprar(carro: carroEntity)
    }
    
    func paginate() {
        
        self.interactor.pagination()
    }
    
    func didSected(id: Int) {
        
        wireframe.createDetail(id: id)
    }
    
    func openCarrinho(){
       
        wireframe.openCarrinho()
    }
    
}

extension HomePresenter: HomeInteractorOutput {
    
    func insertSucess() {
        
        self.output?.hideLoading()
        
        self.output?.addCarrinho()
    }
    
    func insertError() {
        self.output?.hideLoading()
        self.output?.insertError()
    }
    
    func finishPagination() {
        self.output?.didPagination()
    }
    
    func fetched(carrosItem: [CarroItem]) {
        
        self.output?.hideLoading()
        
        self.output?.resultCarros(carros: carrosItem.map({ return CarroMapper.homeListHomeItem(carroItem: $0)}))
    }
    
    func requestError() {
        
        self.output?.hideLoading()
        
        self.output?.error()
    }
    
    
}
