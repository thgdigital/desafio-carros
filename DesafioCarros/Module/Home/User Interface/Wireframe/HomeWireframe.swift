//
//  HomeWireframe.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit
class HomeWireframe: StoryboardInstanciate {
    
    var storyboardName: String = "HomeStoryboard"
    var viewController: UIViewController?
    
    func create() -> UINavigationController {
        
         guard let _viewController = viewControllerFromStoryboard(
            withIdentifier: "HomeViewController") as? HomeViewController else{ return UINavigationController() }
        
        let presenter = HomePresenterBuilder.create(wireframe: self)
        _viewController.presenter = presenter
        
        presenter.output = _viewController
        
        viewController = _viewController
        
        let navigation = UINavigationController(rootViewController: _viewController)
        return navigation
        
    }
    
    func openCarrinho(){
        self.viewController?.navigationController?.pushViewController(CarrinhoWireframe().create(), animated: true)
    }
    
    func createDetail(id: Int){
        viewController?.navigationController?.pushViewController(DetailWireframe().create(id: id), animated: true)
    }
}
