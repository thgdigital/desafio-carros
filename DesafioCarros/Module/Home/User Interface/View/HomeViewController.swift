//
//  ViewController.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import UIKit
import RNActivityView

class HomeViewController:  UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var presenter: HomePresenterInput?
    var items: [CarroDisplay] = [CarroDisplay]()
    let footerReuseIdentifier: String = "footer"
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
 
    fileprivate var loadingItens: Bool = false {
        didSet {
            self.collectionViewLayout.invalidateLayout()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.viewDidLoad()
        
        addRightBarButtonItem(#imageLiteral(resourceName: "carrinho"), target: self, action: #selector(self.widthCarrinho))
        
        self.navigationItem.title = "Lista de Carros"
        
        self.collectionView?.register(CarroCell.self)
        
        self.collectionView!.register(CarroFooter.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter , withReuseIdentifier: footerReuseIdentifier)

    }
    
    @objc func widthCarrinho(){
        self.presenter?.openCarrinho()
    }
    
}

// MARK: - DATASOURCE
extension HomeViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 180)
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CarroCell  = collectionView.dequeueReusableCell(for: indexPath)
        
        cell.delegate = self
        
        if items.count == (indexPath.row + 1){
            
//            cell.lineView.isHidden = true
        }
        
        cell.displayTemplate(display: items[indexPath.row], isHidden: true, index: indexPath.row)
        
        return cell
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let footerView: UICollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerReuseIdentifier, for: indexPath)
        
        if self.loadingItens {
            footerView.clipsToBounds = true
            self.activityView.frame.origin.x = (footerView.frame.width / 2) - self.activityView.frame.width/2
            self.activityView.frame.origin.y = footerView.frame.height / 2 - self.activityView.frame.height/2
            
            footerView.addSubview(self.activityView)
            self.activityView.startAnimating()
        } else {
            self.activityView.removeFromSuperview()
        }
        
        return footerView
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if self.loadingItens {
            return CGSize(width: view.frame.width, height: 40)
        }
        return CGSize(width: view.frame.width, height: 1);
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter?.didSected(id: self.items[indexPath.row].id)
    }
    
    // MARK: - UIScrollViewDelegate
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Request
        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.bounds.height {
             self.loadingItens = true
            self.presenter?.paginate()
        }
        
    }
}
extension HomeViewController: CarroCellDelegate {
    
    func edit(index: Int, qtd: Int) {}
    
   
    func remove(index: Int) {}
    
    
    func didComprar(index: Int, qtd: Int) {
        self.presenter?.comprar(index, qtd: qtd)
    }
    
    
}
extension HomeViewController: HomePresenterOutput {
    func addCarrinho() {
        UIAlertController.showAlert(
            title: "Salvando Dados",
            message: "Dados Inserido com sucesso",
            cancelButtonTitle: "OK",
             viewController: self
        )
    }
    
    func insertError() {
    
        UIAlertController.showAlert(
            title: "Ops Error :(",
            message: "Error ao inserir dados",
            cancelButtonTitle: "OK",
            viewController: self
            
        )
    }
    
    
    func load() {
        self.navigationController?.view.showActivityView()
    }
    
    func resultCarros(carros: [CarroDisplay]) {
        self.loadingItens = false
        self.items = carros
        self.collectionView?.reloadData()
    }
    
    func hideLoading() {
        self.navigationController?.view.hideActivityView()
    }
    
    // TODO: - Tratar mensagem de error
    func error() {
         self.navigationController?.view.hideActivityView()
    }
    
    func didPagination() {
        self.loadingItens = false
        self.collectionView?.reloadData()
    }
    
}
