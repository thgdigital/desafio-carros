//
//  CarrinhoInteractorBuilder.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 16/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation


class CarrinhoInteractorBuilder {
    
    static func create() -> CarrinhoInteractor {

        let managerDB = DBManager.shared
        let interactor = CarrinhoInteractor(manager: managerDB)
        managerDB.output = interactor
        return interactor
    }
}
