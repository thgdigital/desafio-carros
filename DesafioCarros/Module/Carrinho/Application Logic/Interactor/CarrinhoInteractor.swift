//
//  CarrinhoInteractor.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 15/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import CoreData

protocol CarrinhoInteractorInput {
    var itemEntity: [ItemEntity] { get set }
    func fetch()
    func finalizarCompra()
    func removeItem(in index: Int)
    func addItem(item: CarroEntity)
    func editItem(index: Int, qtd: Int)
    func list(item: CarroEntity)
}
protocol CarrinhoInteractorOutput: class {
    func didComprar(status: Bool)
    func fetched(carrosItem: [CarroItem])
    func didModification(status: Bool)
    
}

class CarrinhoInteractor: CarrinhoInteractorInput {
    
    weak var output: CarrinhoInteractorOutput?
    var manager: DBManager
    var itemEntity: [ItemEntity] = [ItemEntity]()
    
    
    init(manager: DBManager) {
        self.manager = manager
    }
    func fetch(){
        guard let _itemEntity =  manager.featchItem() else { return }
        
        let carroEntity = _itemEntity.map({ CarroMapper.listItem(itemEntity: $0)})
        
        self.itemEntity = _itemEntity
        
        self.output?.fetched(carrosItem: carroEntity.map({ CarroMapper.homeListHomeEntity(carroEntity: $0)}))
    }
    
    func removeItem(in index: Int) {
        
        manager.deleteObj(itemEntity[index])
        
    }
    
    func addItem(item: CarroEntity) {}
    
    func editItem(index: Int, qtd: Int){
        let entity = self.itemEntity[index]
        entity.qtd = Int16(qtd)
        self.manager.editObj(entity)
    }
    
    func finalizarCompra(){
        var total = 0
        
        for _entity in itemEntity {
            total = Int(_entity.qtd) * Int(_entity.preco) + total
        }
        let limit = Int(100000)
        
        if total  <=  limit {
            
            itemEntity.forEach { (_itemEntity) in
                let compraEntity = CarroMapper.convertItemEntityWidth(itemEntity: _itemEntity)
                
                manager.saveObjCompra(compraEntity)
                
                manager.deleteObj(_itemEntity)
                
                self.itemEntity = self.itemEntity.filter({ $0.objectID != _itemEntity.objectID })
            }
            
            self.output?.didComprar(status: true)
            
            let carroEntity = self.itemEntity.map({ CarroMapper.listItem(itemEntity: $0)})
            
            self.output?.fetched(carrosItem: carroEntity.map({ CarroMapper.homeListHomeEntity(carroEntity: $0)}))
            
        }else{
            self.output?.didComprar(status: false)
            
            
        }
    }
    
    func list(item: CarroEntity){}
}

extension CarrinhoInteractor: DBManagerOuput{
    
    func didModification(status: Bool) {
        
        self.output?.didModification(status: status)
        
        self.fetch()
        
    }
    
    
}
