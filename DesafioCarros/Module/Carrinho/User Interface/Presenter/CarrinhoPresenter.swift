//
//  CarrinhoPresenter.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 15/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

protocol CarrinhoPresenterInput {
    func viewWillAppear()
    func remover(index: Int)
    func edit(index: Int, qtd: Int)
    func finalizar()
    
}

protocol CarrinhoPresenterOutput: class {
    func resultCarros(carros: [CarroDisplay])
    func load()
    func hideLoading()
    func error()
    func realodView()
    func errorCompra(title: String, messege: String)
    func sucessoCompra(title: String, messege: String)
    
}

class CarrinhoPresenter: CarrinhoPresenterInput {
    
    var wireframe: CarrinhoWireframe
    var interactor: CarrinhoInteractorInput
    weak var output: CarrinhoPresenterOutput?
    
    init(wireframe: CarrinhoWireframe, interactor: CarrinhoInteractor) {
        self.wireframe = wireframe
        self.interactor = interactor
    }
    
    func viewWillAppear(){
        self.interactor.fetch()
        self.output?.load()
    }
    
    func edit(index: Int, qtd: Int){
        
        self.output?.load()
        self.interactor.editItem(index: index, qtd: qtd)
    }
    func finalizar(){
        self.interactor.finalizarCompra()
    }
}
extension CarrinhoPresenter: CarrinhoInteractorOutput{
    
    func didComprar(status: Bool) {
        if status{
            self.output?.errorCompra(title: "Compra finalizada", messege: "Olá sua compra foi finalizada com sucesso :)")
        }else{
            self.output?.errorCompra(title: "Ops Error", messege: "Olá você passou seu limite de comprar. Limite de compras são até R$ 1000.000,00")
        }
    
    }
    
    
    func fetched(carrosItem: [CarroItem]) {
        
        self.output?.hideLoading()
        
        self.output?.resultCarros(carros: carrosItem.map({ return CarroMapper.homeListHomeItem(carroItem: $0)}))
    }
    
    func didModification(status: Bool) {
        
        self.output?.realodView()
        
        self.output?.hideLoading()
    }
    
    func remover(index: Int){
        
        self.output?.load()
        
        self.interactor.removeItem(in: index)
    }
    
    
}
