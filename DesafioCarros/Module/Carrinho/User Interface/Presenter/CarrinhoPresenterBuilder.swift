//
//  CarrinhoPresenterBuilder.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 16/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

class CarrinhoPresenterBuilder {
    
    static func create(wireframe: CarrinhoWireframe) -> CarrinhoPresenter {
        
        let interactor = CarrinhoInteractorBuilder.create()
        
        let presenter = CarrinhoPresenter(wireframe: wireframe, interactor: interactor)
        
        interactor.output = presenter
        
        return presenter
    }
}
