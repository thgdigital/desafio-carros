//
//  CarrinhoWireframe.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 15/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit

class CarrinhoWireframe: StoryboardInstanciate {
    
    var storyboardName: String = "Carrinho"
    
    func create() -> UIViewController {
        
        guard let _viewController = viewControllerFromStoryboard(
            withIdentifier: "CarrinhoViewController") as? CarrinhoViewController else{ return UINavigationController() }
        
        let presenter = CarrinhoPresenterBuilder.create(wireframe: self)
        
        _viewController.presenter = presenter
        
        presenter.output = _viewController
        
        return _viewController
        
    }
}
