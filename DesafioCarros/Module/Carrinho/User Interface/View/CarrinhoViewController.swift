//
//  CarrinhoViewController.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 15/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import UIKit
import RNActivityView
import DZNEmptyDataSet


class CarrinhoViewController:  UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var items: [CarroDisplay] = [CarroDisplay]()
    
    var presenter: CarrinhoPresenterInput?
    
    let footerReuseIdentifier: String = "footer"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Cesta de carros"
        
        self.collectionView?.register(CarroCell.self)
        
        self.collectionView?.emptyDataSetDelegate = self
        self.collectionView?.emptyDataSetSource = self
        
        self.collectionView?.register(UINib(nibName: "CarroFooter", bundle:nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerReuseIdentifier)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.presenter?.viewWillAppear()
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 180)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CarroCell  = collectionView.dequeueReusableCell(for: indexPath)
        
        cell.delegate = self
        
        cell.displayTemplate(display: items[indexPath.row], isHidden: false, index: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if items.count > 0 {
            
            return CGSize(width: view.frame.width, height: 80)
        }
        return CGSize(width: view.frame.width, height: 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerReuseIdentifier, for: indexPath) as! CarroFooter
        
        footerView.delegate = self
        
        return footerView
    }
    
}
extension CarrinhoViewController : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource{
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "carrinho-empty")
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Tá vazio!", attributes:
            [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        return atrributed
        
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Ainda não há nenhum item no seu carrinho.", attributes:
            [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        return atrributed
    }
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Continuar comprando", attributes:
            [NSAttributedStringKey.foregroundColor: UIColor.white])
        return atrributed
    }
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return buttonImageForEmptyState()
    }
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return .clear
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension CarrinhoViewController: CarrinhoPresenterOutput{
    
    func sucessoCompra(title: String, messege: String) {
        UIAlertController.showAlert(
            title: title,
            message: messege,
            cancelButtonTitle: "OK",
            viewController: self
        )
    }
    
    func errorCompra(title: String, messege: String) {
        UIAlertController.showAlert(
            title: title,
            message: messege,
            cancelButtonTitle: "OK",
            viewController: self
        )
    }
    
    
    func load() {
        self.navigationController?.view.showActivityView()
    }
    
    func hideLoading() {
        self.navigationController?.view.hideActivityView()
    }
    
    func error() {
        self.navigationController?.view.hideActivityView()
    }
    func realodView(){
        
    }
    
    func resultCarros(carros: [CarroDisplay]) {
        
        self.items = carros
        collectionView?.reloadData()
    }
    
    func addCarrinho(){
        UIAlertController.showAlert(
            title: "Salvando Dados",
            message: "Dados Inserido com sucesso",
            cancelButtonTitle: "OK",
            viewController: self
        )
    }
    
}

extension CarrinhoViewController: CarroFooterDelegate{
    func finalizar() {
        self.presenter?.finalizar()
    }
    
    
}

extension CarrinhoViewController: CarroCellDelegate {
    
    func edit(index: Int, qtd: Int) {
        self.presenter?.edit(index: index, qtd: qtd)
    }
    
    
    func didComprar(index: Int, qtd: Int) {}
    
    func remove(index: Int) {
        self.presenter?.remover(index: index)
    }
    
    
    
    
}
