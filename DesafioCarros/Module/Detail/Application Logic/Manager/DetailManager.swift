//
//  DetailManager.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol DetailManagerInput {
    func fecth(id: Int)
}
protocol DetailManagerOutput: class {
    func fecthed(carrosEntity: CarroEntity)
    func requestError()
    func fecthedPaginate()
}

class DetailManager: DetailManagerInput {
    
    weak var output: DetailManagerOutput?
    
    func fecth(id: Int) {
        let url = assembleURL(endpoint: .carro, id: id)
        _ = Alamofire.request(url, method: .get).validate().responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                guard let jsonObject = response.result.value else {
                    self.output?.requestError()
                    return
                }
                let json                = JSON(jsonObject)
                
                self.output?.fecthed(carrosEntity: CarroMapper.homeListHomeEntity(json: json))
    
                
            case .failure( _):
                self.output?.requestError()
                
            }
        }
        
    }
    // MARK: - Assemble URL
    internal func assembleURL(endpoint: EndPointer, id: Int) -> String {
        let  url = "\(App.baseURL)/\(endpoint.rawValue)/\(id)"
        return url
    }
    
}
