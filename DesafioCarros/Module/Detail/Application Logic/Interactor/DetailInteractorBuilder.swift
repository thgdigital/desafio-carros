//
//  DetailInteractorBuilder.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

class DetailInteractorBuilder {
    
    static func create() -> DetailInteractor {
        
        let manager = DetailManager()
        let managerDB = DBManager.shared
        let interactor = DetailInteractor(manager: manager, managerDB: managerDB)
        manager.output = interactor
        managerDB.output = interactor
        return interactor
    }
}
