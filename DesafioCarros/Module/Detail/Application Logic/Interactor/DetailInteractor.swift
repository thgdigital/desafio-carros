//
//  DetailInteractor.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

protocol DetailInteractorInput {
    func fetch(id: Int)
    func comprar(qtd: Int)
}
protocol DetailInteractorOutput: class {
    func fetched(carrosItem: CarroItem)
    func finishPagination()
    func modification(status: Bool)
    func requestError()
}
class DetailInteractor: DetailInteractorInput {
    
    var manager: DetailManager
    var managerDB: DBManager
    var carrosEntity: CarroEntity = CarroEntity()
    
    weak var output: DetailInteractorOutput?
    
    init(manager: DetailManager, managerDB: DBManager) {
        self.manager = manager
        self.managerDB = managerDB
    }
    func fetch(id: Int) {
        self.manager.fecth(id: id)
    }
    
    func comprar(qtd: Int){
    carrosEntity.qtd = qtd
        managerDB.saveObj(CarroMapper.convertItemWidthCarroEntity(carroEntity: carrosEntity))
    }
}

extension DetailInteractor: DBManagerOuput {
    
    func didModification(status: Bool) {
        self.output?.modification(status: status)
    }
}

extension DetailInteractor: DetailManagerOutput {
    func fecthed(carrosEntity: CarroEntity) {
        self.carrosEntity = carrosEntity
        self.output?.fetched(carrosItem: CarroMapper.homeListHomeEntity(carroEntity: self.carrosEntity))
    }
    
    func requestError() {
        self.output?.requestError()
    }
    
    func fecthedPaginate() {
        self.fecthedPaginate()
    }
    
    
}
