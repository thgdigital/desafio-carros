//
//  DetailPresenterBuilder.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

class DetailPresenterBuilder {
    
    static func create(wireframe: DetailWireframe, id: Int) -> DetailPresenter {
        
        let interactor = DetailInteractorBuilder.create()
        let presenter = DetailPresenter(wireframe: wireframe, interactor: interactor)
        presenter.id = id
        interactor.output = presenter
        
        return presenter
        
    }
}
