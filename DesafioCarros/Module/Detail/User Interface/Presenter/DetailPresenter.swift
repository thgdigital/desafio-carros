//
//  DetailPresenter.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation

protocol DetailPresenterInput {
    func viewdidLoad()
    func setQtd(qtd: Int)
    func comprar()
}
protocol DetailPresenterOutput: class {
    func load()
    func resultCarros(carro: CarroDisplay)
    func didPagination()
    func hideLoading()
    func error()
}
class DetailPresenter: DetailPresenterInput {
  
    var wireframe: DetailWireframe
    var interactor: DetailInteractorInput
    weak var output: DetailPresenterOutput?
    var id: Int = 0
    var qtd: Int = 1
    
    init(wireframe: DetailWireframe, interactor: DetailInteractorInput) {
        
        self.wireframe = wireframe
        self.interactor = interactor
    }
    func viewdidLoad() {
        self.output?.load()
        self.interactor.fetch(id: id)
    }
    func setQtd(qtd: Int) {
        self.qtd = qtd
    }
    
    func comprar() {
        self.interactor.comprar(qtd: self.qtd)
        
    }
}

extension DetailPresenter: DetailInteractorOutput{
    
    func modification(status: Bool) {
        
    }
    
    
    
    func fetched(carrosItem: CarroItem) {
        self.output?.hideLoading()
        self.output?.resultCarros(carro: CarroMapper.homeListHomeItem(carroItem: carrosItem))
    }
    
    func finishPagination() {
        self.output?.didPagination()
    }
    
    func requestError() {
        self.output?.error()
    }
    
    
}



