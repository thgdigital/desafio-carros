//
//  DetailWireframe.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit
class DetailWireframe: StoryboardInstanciate {
    
    var storyboardName: String = "DetailStoryboard"
    
    func create(id: Int) -> DetailViewController {
        
        let viewController = viewControllerFromStoryboard(
            withIdentifier: "DetailViewController") as! DetailViewController
        let presenter = DetailPresenterBuilder.create(wireframe: self, id: id)
        viewController.presenter = presenter
        presenter.output = viewController
     
        return viewController
        
    }
    
    
}
