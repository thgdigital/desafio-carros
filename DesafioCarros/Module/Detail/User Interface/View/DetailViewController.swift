//
//  DetailViewController.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import UIKit
import RNActivityView

class DetailViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var qtdSepper: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var precoLabel: UILabel!
    
    
    var presenter: DetailPresenterInput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewdidLoad()
        sendButton.layer.cornerRadius = 5
        navigationItem.title = "Detalhes dos Carros"
        setupNavigationBarBackButton()
    }

    
    
    @IBAction func sendComprar(_ sender: Any) {
        self.presenter?.comprar()
    }
    
    @IBAction func stepperSend(_ sender: UIStepper) {
        let qtD  = Int(sender.value)
        self.presenter?.setQtd(qtd: qtD)
        self.qtdSepper.text = "QTD: \(qtD)"
    }
}

extension DetailViewController: DetailPresenterOutput {
    func load() {
        self.navigationController?.view.showActivityView()
    }
    
    func resultCarros(carro: CarroDisplay) {
        
        let title = "\(carro.marca) \(carro.name)"
        self.navigationItem.title = title
        titleLabel.text = title
        subTitle.text  = carro.descricao
        precoLabel.text = carro.preco
        imageView.sd_setImage(with: URL(string: carro.image), placeholderImage: #imageLiteral(resourceName: "placeholder"))
    }
    
    func didPagination() {}
    
    func hideLoading() {
       self.navigationController?.view.hideActivityView()
    }
    
    func error() {
        self.navigationController?.view.hideActivityView()
    }
    
    
}
