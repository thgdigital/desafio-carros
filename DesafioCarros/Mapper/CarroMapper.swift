//
//  HomeMapper.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

class CarroMapper: NSObject {
    
    static func homeListHomeEntity(json: JSON) -> CarroEntity {
        
        let carro = CarroEntity()
        carro.id = json["id"].intValue
        carro.name = json["nome"].stringValue
        carro.marca = json["marca"].stringValue
        carro.descricao = json["descricao"].stringValue
        carro.quantidade = json["quantidade"].intValue
        carro.preco = json["preco"].intValue
        carro.image = json["imagem"].stringValue

        return carro
    }
    static func homeListHomeEntity(carroEntity: CarroEntity) -> CarroItem {
 
        var carroItem = CarroItem()
        carroItem.id = carroEntity.id
        carroItem.name = carroEntity.name
        carroItem.descricao = carroEntity.descricao
        carroItem.marca = carroEntity.marca
        carroItem.quantidade = carroEntity.quantidade
        carroItem.preco = carroEntity.preco
        carroItem.image = carroEntity.image
        carroItem.qtd = carroEntity.qtd

        return carroItem
    }
    
    static func homeListHomeItem(carroItem: CarroItem) -> CarroDisplay {
        
        var carroDisplay = CarroDisplay()
            carroDisplay.id = carroItem.id
            carroDisplay.name = carroItem.name
            carroDisplay.descricao = carroItem.descricao
            carroDisplay.marca = carroItem.marca
            carroDisplay.preco = carroItem.preco.CGFloatValue.currencyBR
            carroDisplay.image = carroItem.image
            carroDisplay.qtd = carroItem.qtd
            
        return carroDisplay
    }
    
    static func listItem(itemEntity: ItemEntity) -> CarroEntity {
        let carroEntity = CarroEntity()
        carroEntity.id = Int(itemEntity.idProduto)
        carroEntity.name = itemEntity.name ?? ""
        carroEntity.marca = itemEntity.marca ?? ""
        carroEntity.descricao = itemEntity.descricao ?? ""
        carroEntity.preco = Int(itemEntity.preco)
        carroEntity.image = itemEntity.image ?? ""
    
        carroEntity.qtd = Int(itemEntity.qtd)

        return carroEntity
    }
    static func convertItemWidthCarroEntity(carroEntity: CarroEntity) -> ItemEntity {
        let itemEntity = DBManager.shared.new("ItemEntity") as! ItemEntity
        itemEntity.idProduto = Int16(carroEntity.id)
        itemEntity.name = carroEntity.name
        itemEntity.marca = carroEntity.marca
        itemEntity.descricao = carroEntity.descricao
        itemEntity.preco = Int16(carroEntity.preco)
        itemEntity.image = carroEntity.image
        itemEntity.qtd = Int16(carroEntity.qtd)
        
        return itemEntity
    }
    
    static func convertItemEntityWidth(itemEntity: ItemEntity) -> ComprarItemEntity {
         let comprarItemEntity = DBManager.shared.new("ComprarItemEntity") as! ComprarItemEntity
        comprarItemEntity.id = itemEntity.idProduto
        comprarItemEntity.name = itemEntity.name
        comprarItemEntity.marca = itemEntity.marca
        comprarItemEntity.descricao = itemEntity.descricao
        comprarItemEntity.preco = itemEntity.preco
        comprarItemEntity.image = itemEntity.image
        
        comprarItemEntity.qtd =  itemEntity.qtd
        
        return comprarItemEntity
    }
}
