//
//  HomeCell.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 14/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage

protocol CarroCellDelegate {
    func didComprar(index: Int, qtd: Int)
    func remove(index: Int)
    func edit(index: Int, qtd: Int)
}

class CarroCell: CollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var precoLabel: UILabel!
    @IBOutlet weak var qtdLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var removeButton: UIButton!
    
    @IBOutlet weak var stepper: UIStepper!
    
    
    @IBOutlet weak var lineView: UIView!
    
    var delegate: CarroCellDelegate?
    var carro: CarroDisplay?
    var qtd: Int = 1
    var index: Int = 0
    
    
    @IBAction func remover(_ sender: Any) {
        
        self.delegate?.remove(index: self.index)
    }
    
    
    @IBAction func sendCompra(_ sender: Any) {
    
        delegate?.didComprar(index: self.index, qtd: self.qtd)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sendButton.layer.cornerRadius = 5
        
        self.removeButton.layer.cornerRadius = 5
        
        titleLabel.text = ""
        
        subTitle.text = ""
        
        precoLabel.text = ""
        
        qtdLabel.text = ""
    }

    override func displayTemplate(display: Any, isHidden: Bool, index: Int) {
        
        guard let carro = display as? CarroDisplay else{ return }
        
        self.carro = carro
        
        self.index = index
        
        self.qtd = carro.qtd == 0 ? 1 : carro.qtd
        
        imageView.sd_setImage(with: URL(string: carro.image), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        
        titleLabel.text = "\(carro.marca) \(carro.name)"
        
        subTitle.text = carro.descricao
        
        precoLabel.text = carro.preco
        
        qtdLabel.text = "QTD: \(qtd)"
        
        stepper.value = Double(qtd > 0 ? qtd : 1)
        
        self.stepper.isHidden = isHidden
        
        self.qtdLabel.isHidden = isHidden
        
        self.removeButton.isHidden = isHidden
        
        self.sendButton.isHidden = !isHidden
    }
    
    @IBAction func stepperSend(_ sender: UIStepper) {
        
        qtd  = Int(sender.value)
        
        self.delegate?.edit(index: self.index, qtd: self.qtd)
        
        self.qtdLabel.text = "QTD: \(qtd)"
    }
}
