//
//  CarroFooter.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 16/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import UIKit

//
//  CarroFooter.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 16/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit

protocol CarroFooterDelegate {
    func finalizar()
}

class CarroFooter: UICollectionReusableView {
    
    var delegate: CarroFooterDelegate?
    
    @IBOutlet weak var finalizar: UIButton!
    
    @IBAction func sendFinalizar(_ sender: Any) {
        
        self.delegate?.finalizar()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        finalizar.isHidden = false
        finalizar.layer.cornerRadius = 5
    }
    
    class func instanciateFromNib() -> CarroFooter {
        return Bundle.main.loadNibNamed("CarroFooter", owner: nil, options: nil)![0] as! CarroFooter
    }
}

