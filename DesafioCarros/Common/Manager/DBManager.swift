//
//  DBManager.swift
//  DesafioCarros
//
//  Created by Thiago Santos on 15/06/2018.
//  Copyright © 2018 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol DBManagerOuput: class {
    func didModification(status: Bool)
    
}


class DBManager {
    
    static let shared = DBManager()
    weak var output: DBManagerOuput?
    
    var entityObj: NSManagedObject!   //Recebe o objeto do managedContext
    
    lazy var managedContext: NSManagedObjectContext = {
        var appDelegate = UIApplication.shared.delegate as! AppDelegate
        var c = appDelegate.managedObjectContext
        return c
    }()
    
    //MARK: - CRUD -
    
    /// Cria nova entidade no managedContext
    func new(_ entityName: String) -> NSManagedObject{
        return NSEntityDescription.insertNewObject(forEntityName: entityName, into: managedContext)
    }
    
    func saveObj(_ obj: NSManagedObject) {
        do {
            try obj.managedObjectContext?.save()
            self.output?.didModification(status: true)
        } catch let error as NSError {
            self.output?.didModification(status: false)
            print("Could not save. Error: \(error), \(error.userInfo)")
        }
    }
    func saveObjCompra(_ obj: NSManagedObject) {
        do {
            try obj.managedObjectContext?.save()
        } catch let error as NSError {
            print("Could not save. Error: \(error), \(error.userInfo)")
        }
    }
    
    /// Delete an object
    func deleteObj(_ obj: NSManagedObject) {
        
        managedContext.delete(obj)
        
        do {
            try managedContext.save()
            self.output?.didModification(status: true)
        } catch let error as NSError {
            self.output?.didModification(status: false)
            print("Could not delete. Error: \(error), \(error.userInfo)")
        }
    }
    
    /// Edit an object
    func editObj(_ obj: ItemEntity) {
        
        obj.setValue(obj.qtd, forKey: "qtd")
        
        do {
            try managedContext.save()
            self.output?.didModification(status: true)
        } catch let error as NSError {
            self.output?.didModification(status: false)
            print("Could not delete. Error: \(error), \(error.userInfo)")
        }
    }
    
    func featchItem() -> [ItemEntity]?{
        let predicate = NSPredicate(value: true)
        let ordeby = NSSortDescriptor(key: "name", ascending: true)
        
        guard  let _itens =   DBManager.shared.customSearch("ItemEntity",  predicate: predicate, sortDescriptor: ordeby) as? [ItemEntity] else {
            
            return nil
        }
        return _itens
    }
    
    /// Custom - All searches can use it
    func customSearch(_ entityName: String, predicate: NSPredicate, sortDescriptor: NSSortDescriptor) -> [NSManagedObject]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        fetchRequest.sortDescriptors = [sortDescriptor]  // Sort descriptor object that sorts ( ORDER BY )
        fetchRequest.predicate = predicate               // Predicate filters out ( WHERE )
        
        var fetchedResults: [NSManagedObject]?
        
        do {
            try fetchedResults = self.managedContext.fetch(fetchRequest) as? [NSManagedObject]
        } catch let error as NSError {
            print("Error: \(error)")
        }
        
        return fetchedResults
    }
}
